var Client = require('bittorrent-tracker')
var magnet = require('magnet-uri')

function scrape(magnetURI, callback)
{
    var parsed = magnet(magnetURI);

    var requiredOpts = {
      infoHash: parsed.infoHashBuffer,
      peerId: new Buffer('01234567890123456789'),
      announce: parsed.tr
    }

    var client = new Client(requiredOpts);

    client.scrape();

    client.on('scrape', function(data)
    {
        callback(data);
    });
}

window['scrape'] = scrape;
